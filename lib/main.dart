import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

///widget ini biasa digunakan untuk membuat tampilan
///ui seperti halnya sebuah chat
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'List Tile',
          ),
        ),
        body: ListView(
          children: [
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
              title: Text('Mark Zukerbek'),
              subtitle: Text(
                'hello david welcome backkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              trailing: Text('10.00 PM'),
              leading: CircleAvatar(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://th.bing.com/th/id/R.afffcfa010b11f6e110b5f66efe273dd?rik=m%2bAYXmXTkIVJpg&riu=http%3a%2f%2f3.bp.blogspot.com%2f-mMPYOZU5pu0%2fTrNfPes5seI%2fAAAAAAAAAEM%2fnSLAKmNsRQw%2fs1600%2ff2dbb95343_35625196_o2.jpg&ehk=%2boQwRqbTFBsBJcV5BV6%2fOjGc8XmWi%2bRkH%2bk7%2fjTuqS8%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1'),
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            Divider(color: Colors.black)
          ],
        ),
      ),
    );
  }
}
